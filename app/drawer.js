import React, { Component } from 'react';
import { View } from 'react-native';
import Drawer from 'react-native-drawer';
import {Actions, DefaultRenderer} from 'react-native-router-flux';
import SideMenu from './components/drawer/sidemenu_v2.js';


export default class extends Component {
  render(){
      const state = this.props.navigationState;
      const children = state.children;
      return (
          <Drawer
              ref="navigation"
              open={state.open}
              onOpen={()=>Actions.refresh({key:state.key, open: true})}
              onClose={()=>Actions.refresh({key:state.key, open: false})}
              type="displace"
              content={
                <SideMenu
                  onClose={()=>Actions.refresh({key:state.key,open: false})}
                  changeLang={this.props.changeLang.bind(this)}
                />
              }
              tapToClose={true}
              openDrawerOffset={0}
              duration={2}
              panCloseMask={0}
              negotiatePan={true}
              tweenHandler={(ratio) => ({
               main: { opacity:Math.max(0.54,1-ratio) }
          })}>
              <DefaultRenderer navigationState={children[0]} onNavigate={this.props.onNavigate} />
          </Drawer>
      );
  }
}
