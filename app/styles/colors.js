const primary = '#089CC5';
const secondary = '#fff';
const accent = '#000';

const primaryText = '#fff';
const secondaryText= '#CBCBCD'; //gray
const darkText = '#000';
const accentText = '#F0C238';

const darkBorder = '#585858';

//theme: status bar, app bar, backgroung, card, dialogs
//
export {
  primary,
  secondary,
  accent,
  primaryText,
  secondaryText,
  darkText,
  accentText,
  darkBorder
}