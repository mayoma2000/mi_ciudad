import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet,Image, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import EStyleSheet from 'react-native-extended-stylesheet';

const DrawerCard = ({img, label, onPress}) => (
  <TouchableOpacity 
    onPress={onPress}
    style={styles.container}
  >
    <View>
      <Image style={styles.icon} source={img()}/>
      <Text style={styles.iconText}>{label()}</Text>
    </View>
  </TouchableOpacity>
);

const styles = EStyleSheet.create({
  icon: {
    resizeMode: 'contain', 
    width: '7.25rem', 
    height: '6.25rem'
  },
  iconText: {
    fontFamily: 'Gotham', 
    textAlign:'center', 
    color: '#fff', 
    fontSize: '1.12rem', 
    marginTop: 5
  },
  container: {
    width: Dimensions.get('window').width * 0.40, 
    marginBottom: 8, 
    padding: 0, 
    marginHorizontal: 10, 
    justifyContent: 'center', 
    alignItems: 'center'
  }
});

export default DrawerCard;