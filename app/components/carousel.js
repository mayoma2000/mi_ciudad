import React, {Component} from 'react';
import {View,Text,TouchableOpacity,ScrollView,Image,Dimensions } from "react-native"

export default class Carousel extends Component{

  constructor(props){
    super(props)
    this.width = Dimensions.get('window').width
    this.state={
      defaultImageIndex:this.props.defaultImageIndex,
      enableToChange:true,
    }

  }

  changeImageIndex(defaultImageIndex){
    this.setState({defaultImageIndex})
  }

  createImages(elt,key){
    const {joinFirst,getImageFrom,joinLast} = this.props
    return(
      <TouchableOpacity
        key={key}
        style={{height: 75}}
        onPress={()=>{this.changeImageIndex(key)} }
      >
        <Image style={{height: 75, width: 130}} source={{uri:`${joinFirst}${elt[getImageFrom]}${joinLast}`}} />
      </TouchableOpacity>
    )
  }

  createImages2(elt,key){
    const {joinFirst,getImageFrom,joinLast} = this.props
    return(
      <TouchableOpacity
        key={key}
        style={{width: null, height: 330,flex:1}}
        onPress={()=>{this.changeImageIndex(key)} }
      >
        <Image style={{width: 330, height: 330}} source={{uri:`${joinFirst}${elt[getImageFrom]}${joinLast}`}} />
      </TouchableOpacity>
    )
  }

  render(){
    const {dataImage:data,joinFirst,joinLast,getImageFrom} = this.props;
    const {mainImageComponents} = this.props;
    const {defaultImageIndex}= this.state;
    return(
      <View>

        <View style={{height: 280, padding: 0}}>

            <Image source={{uri:`${joinFirst}${data[defaultImageIndex][getImageFrom]}${joinLast}`}}
             style={{width: this.width, height: 280,flex:1}}
             resizeMode='cover'>
              {mainImageComponents}
           </Image>

        </View>

        <View>
          <ScrollView
            ref={(scrollView) => { _scrollView = scrollView; }}
            automaticallyAdjustContentInsets={false}
            onScroll={() => { console.log('onScroll!'); }}
            scrollEventThrottle={200}
            horizontal={true}
            style={{height: 75}}>
            {data.map((elt, key) => this.createImages(elt, key))}
          </ScrollView>
        </View>
      </View>
    )
  }
}

Carousel.defaultProps={
  joinLast:'',
  joinFirst:'',
  defaultImageIndex:0,
  mainImageComponents:<View/>,
};
